# Talking Head Puppeteering Defense




## Name
Defending Low-Bandwidth Talking Head Videoconferencing Systems From Real-Time Puppeteering Attacks

## Abstract
Talking head videos have gained significant attention in recent years due to advances in AI that allow for the synthesis of realistic videos from only a single image of the speaker. Recently, researchers have proposed low bandwidth talking head video systems for use in applications such as videoconferencing and video calls. However, these systems are vulnerable to puppeteering attacks, where an attacker can control a synthetic version of a different target speaker in real-time. This can be potentially used spread misinformation or committing fraud. Because the receiver always creates a synthetic video of the speaker, deepfake detectors cannot protect against these attacks. As a result, there are currently no defenses against puppeteering in these systems. In this paper, we propose a new defense against puppeteering attacks in low-bandwidth talking head video systems by utilizing the biometric information inherent in the facial expression and pose data transmitted to the receiver. Our proposed system requires no modifications to the video transmission system and operates with lowcomputational cost. We present experimental evidence to demonstrate the effectiveness of our proposed defense and provide a new dataset for benchmarking defenses against puppeteering attacks.

## Publication
The paper is to be published at Workshop on Media Forensics at CVPR2023.

## The Code for the Project
The code for this project is still being finalized.

## Dataset
The Dataset is available at this Google Drive link:
[Google Drive](https://drive.google.com/drive/folders/1l1_xuDx14N7Bs4r3ypb39ePfqTQJ1ENf?usp=drive_link)



## Authors and acknowledgment
Danial Samadi Vahdati, Tai Duc Nguyen, Matthew C. Stamm


